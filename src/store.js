import { createStore, compose, applyMiddleware } from 'redux';
import { createDataLoaderMiddleware } from 'redux-dataloader';

import rootReducer from './rootReducer';
import dataloaders from './dataLoaders/vendorDataLoader';


const dataLoaderMiddleware = createDataLoaderMiddleware(dataloaders);

export default function configureStore(){
    return createStore(
        rootReducer, 
        compose(applyMiddleware(dataLoaderMiddleware)
    ));
}