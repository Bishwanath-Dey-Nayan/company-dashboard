import React, { Component } from 'react';
import { faCircleNotch, faArrowRight, faCheckCircle, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './queueForm.modules.css'
import moment from 'moment'
import _ from 'lodash'

class queueForm extends Component{

    state = {
        response: ''
    }

    render(){
        console.log(!_.isEmpty(this.props.vendorObject))
        return (
            <div >
                <div className="row">
                    <div className="col-md-7 offset-1 mt-3">
                        <h2 className="font-weight-bold" style={{display: 'inline-block'}}>{this.state.response}</h2>
                    </div>
                    <div className="col-md-2 offset-1 mt-4">
                        <button type="button" onClick={this.props.showRegistrationFromQueue} className="btn btn-outline-primary btn-sm" style={{borderRadius: '20px', display: 'inline-block', fontWeight: '700', marginLeft: '57px', marginBottom: '10px', paddingRight: '20px'}}><FontAwesomeIcon icon={faPlus} style={{fontSize:'12px', marginLeft: '3px', marginRight: '8px'}}/>Add Vendor</button>
                    </div>
                </div>
    <div className="col-md-12 offset-1 mt-5">
        <h4 className="font-weight-bold">To Be Processed &nbsp;<FontAwesomeIcon icon={faCircleNotch} style={{fontSize:'10px', marginBottom: '4px'}}/>&nbsp;<FontAwesomeIcon icon={faCircleNotch} style={{fontSize:'10px', marginBottom: '4px'}}/>&nbsp;<FontAwesomeIcon icon={faCircleNotch} style={{fontSize:'10px', marginBottom: '4px'}}/></h4>
    </div>
            <div className="col-md-10 offset-1">
                <table className="table table-hover table-borderless">
                   
                    <tbody>
                        <tr>
                            <th>Company</th>
                            <th>Organization Type</th>
                            <th>Form Author</th>
                            <th>Date Submitted</th>
                            <th></th>
                        </tr>
                        {(!_.isEmpty(this.props.vendorObject)) ? (this.props.vendorObject.map(res => { 
                            if (res) {
                                if(res.isProcessed === 0){ 
                                    return(
                                        <tr className="table-warning" onClick={() => this.props.showVendorForm(res)}>
                                            <td>{res.companyName}</td>
                                            <td>{res.organizationType}</td>
                                            <td>@Baskind,Tom</td>
                                            <td>{moment(res.updateDate).format("DD-MM-YYYY")}</td>
                                            <td><i style={{cursor:'pointer'}}><FontAwesomeIcon icon={faArrowRight} style={{fontSize:'17px', color:'#292b2d'}}/></i></td>
                                        </tr>)
                                }
                            }
                        }
                    )):(
                            <div></div>
                    )
                    }            
                    </tbody>                 
                </table>
                {(_.isEmpty(this.props.vendorObject)) && (<div style={{textAlign: 'center', height: '50px', paddingTop: '10px', fontSize: '20px', background: '#f7f7f7'}}>Seems like there's no vendors to be processed. <b onClick={this.props.showRegistrationFromQueue} style={{textDecoration:'underline', fontWeight: '600', cursor: 'pointer'}}>Add a new vendor</b></div>)}
            </div>
            <div className="col-md-12 offset-1 mt-5">
                    <h4 className="font-weight-bold">Processed Forms<FontAwesomeIcon icon={faCheckCircle} style={{fontSize:'20px', color:'#292b2d'}}/> </h4>
                </div>
            <div className="col-md-10 offset-1">
                <table className="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <th>Company</th>
                            <th>Vendor Number</th>
                            <th>New/Change</th>
                            <th>Date Submitted</th>
                            <th></th>
                        </tr>
                        {(!_.isEmpty(this.props.vendorObject)) ? (this.props.vendorObject.map(res => {
                                if(res.isProcessed === 1){ 
                                    return(
                                        <tr className="table-success" onClick={() => this.props.showVendorForm(res)}>
                                            <td>{res.companyName}</td>
                                            <td>{res.organizationType}</td>
                                            <td>@Baskind,Tom</td>
                                            <td>{res.updateDate.split(' ')[0]}</td>
                                            <td><i style={{cursor:'pointer'}}><FontAwesomeIcon icon={faArrowRight} style={{fontSize:'17px', color:'#292b2d'}}/></i></td>
                                        </tr>)
                                    }
                                }
                                )):   (
                                <div></div>
                            )
                        }
                    </tbody>
                </table>
                {(_.isEmpty(this.props.vendorObject)) && (<div style={{textAlign: 'center', height: '50px', paddingTop: '10px', fontSize: '20px', background: '#f7f7f7'}}>Seems like there's no vendors to be processed. <b onClick={this.props.showRegistrationFromQueue} style={{textDecoration:'underline', fontWeight: '600', cursor: 'pointer'}}>Add a new vendor</b></div>)}
            </div>           
            </div>
            );
    }
}

export default queueForm;