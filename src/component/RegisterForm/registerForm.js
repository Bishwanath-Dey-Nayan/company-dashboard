import React, { Component } from 'react';
import * as  vendorActions from '../../actions/vendorData'
import { faArrowLeft, faPaperclip } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './registerForm.modules.css';
import Form from 'react-bootstrap/FormGroup';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
class registerForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FormTitle: 'New Vendor Form',
            vendorPostObject:{
            taxIdNumber: '',
            organizationType: '',
            contactPerson:'',
            businessPhone: '',
            fax: '',
            email: '',
            remittanceAddress:'',
            remittanceCity: '',
            remittanceState: '',
            remittanceZip: '',
            companyName: '',city: '',mailingAddress: '',zip: '',state: '', updateDate: new Date(), updateUser: 'asdsa'},
            vendorUpdateObject:{
                id: 0,
                taxIdNumber: '',
                organizationType: '',
                contactPerson:'',
                businessPhone: '',
                fax: '',
                email: '',
                remittanceAddress:'',
                remittanceCity: '',
                remittanceState: '',
                remittanceZip: '',
                companyName: '',city: '',mailingAddress: '',zip: '',state: '', updateDate: new Date(), updateUser: 'asdsa'},
            
            Borders: {
                companyNameBorderColor: '#ced4da',   
                cityBorderColor: '#ced4da',          
                mailingAddressBorderColor: '#ced4da',    
                zipBorderColor: '#ced4da', 
                stateBorderColor: '#ced4da',
                emailBorderColor: '#ced4da'}
        }
    }
    componentDidMount() {
        // Additionally I could have just used an arrow function for the binding `this` to the component...
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions);
        document.body.style.margin = "0px";

        if(this.props.VendorData){
            console.log(this.props.VendorData)
            this.setState({vendorPostObject: this.props.VendorData, FormTitle: 'Edit Vendor'});
            this.setState({vendorUpdateObject: this.props.VendorData});
        }
      }
      updateDimensions = () => {
        try{
          var distance = document.querySelector("#registerContainer").getBoundingClientRect().left;
          document.getElementById("VendorFormsHeader").style.marginLeft = (distance + 50).toString() + "px";
          document.getElementById("VendorBackButton").style.marginLeft = (distance - 30).toString() + "px";
        }
        catch(error)
        {
          console.log(error);
        }
      }
    render(){
        
        return (
            <div>         
            <div className="body-wrapper">
            <div style={{backgroundImage: 'linear-gradient(-90deg, #F2994A, #F2C94C)', position: 'relative', height: '120px'}}>
                <button id='VendorBackButton' type="button" className="btn btn-outline-light btn-sm" onClick={this.props.showQueue} style={{borderRadius: '20px', paddingRight: '10px', position: 'absolute', bottom: '10px'}}><FontAwesomeIcon icon={faArrowLeft} style={{fontSize:'14px', marginRight: '5px'}}/> Back</button>
                <h2 id='VendorFormsHeader'  style={{fontFamily: 'Lubali', color: '#fff', fontWeight: '700', position: 'absolute', bottom: '0'}}>{this.state.FormTitle}</h2>
            </div>
                <Form onSubmit={this.ValidateForm} className='registerForm'>
                    <div className="container" id="registerContainer">                    
                        <div className="row mt-4">
                            <div className="col-md-4">
                                <label className="font-weight-bold">Tax ID Number</label>
                                <input type="text" name="taxIdNumber"  onChange={(event) => this.handleUserInput(event)} value={this.state.vendorPostObject.taxIdNumber} className="form-control form-rounded" />
                            </div>
                            <div className="col-md-7">
                                <label className="font-weight-bold">Organization Type</label><br/>
                                <div className="row mt-1">
                                    <div className="col-md-4">
                                        <input type='radio' onChange={(event) => this.handleChange(event)} checked={this.state.vendorPostObject.organizationType === 'Corporation'} value='Corporation' name='organizationType'/><label style={{paddingLeft: '5px'}}>Corporation</label>
                                    </div>
                                    <div className="col-md-5">
                                        <input type='radio' onChange={(event) => this.handleChange(event)} checked={this.state.vendorPostObject.organizationType === 'Individual/Sole Propietor'} value='Individual/Sole Propietor' name='organizationType'/><label style={{paddingLeft: '5px'}}>Individual/Sole Propietor </label>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-4">
                                        <input type='radio' onChange={(event) => this.handleChange(event)} checked={this.state.vendorPostObject.organizationType === 'LLC'} value= 'LLC' name='organizationType'/><label style={{paddingLeft: '5px'}}>LLC</label>
                                    </div>
                                    <div className="col-md-5">
                                        <input type='radio' onChange={(event) => this.handleChange(event)} checked={this.state.vendorPostObject.organizationType === 'Partnership/Limited Partnership'} value='Partnership/Limited Partnership' name='organizationType'/><label style={{paddingLeft: '5px'}}>Partnership/Limited Partnership</label>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-md-4">
                                <label className="font-weight-bold">Name of Company<b style={{color:"#ff2605"}}>*</b></label>
                                <input type="text" className="form-control form-rounded" style={{borderColor:this.state.Borders.companyNameBorderColor}} name='companyName' value={this.state.vendorPostObject.companyName} onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                            <div className="col-md-7">
                            <label className="font-weight-bold">Mailing Address<b style={{color:"#ff2605"}}>*</b></label>
                            <input type="text" className="form-control form-rounded" name='mailingAddress' style={{borderColor:this.state.Borders.mailingAddressBorderColor}} value={this.state.vendorPostObject.mailingAddress} onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-md-4">
                            <label className="font-weight-bold">Contact Person</label>
                            <input type="text" name="contactPerson" value={this.state.vendorPostObject.contactPerson} onChange={(event) => this.handleUserInput(event)} className="form-control form-rounded"/>                            </div>
                            <div className="col-md-4">
                            <label className="font-weight-bold">City<b style={{color:"#ff2605"}}>*</b></label>
                            <input type="text" className="form-control form-rounded" name='city' style={{borderColor:this.state.Borders.cityBorderColor}} value={this.state.vendorPostObject.city} onChange={(event) => this.handleUserInput(event)} />
                            </div>
                            <div className="col-md-3">
                            <label className="font-weight-bold">State<b style={{color:"#ff2605"}}>*</b></label>
                            <select title="Pick a number" className="form-control form-rounded" name='state' style={{borderColor:this.state.Borders.stateBorderColor}} value={this.state.vendorPostObject.state} onChange={(event) => this.handleUserInput(event)}>
                                <option>Select...</option>
                                <option>One</option>
                                <option>Two</option>
                                <option>Three</option>
                            </select>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-md-2">
                            <label className="font-weight-bold">Business Phone</label>
                            <input type="text" value={this.state.vendorPostObject.businessPhone} onChange={(event) => this.handleUserInput(event)} name="businessPhone" className="form-control form-rounded"/>
                            </div>
                            <div className="col-md-2">
                            <label className="font-weight-bold">Fax</label>
                            <input type="text" name="fax" value={this.state.vendorPostObject.fax} onChange={(event) => this.handleUserInput(event)} className="form-control form-rounded"/>
                            </div>
                            <div className="col-md-4">
                            <label className="font-weight-bold">Email</label>
                            <input type="text" name="email" value={this.state.vendorPostObject.email} style={{borderColor:this.state.Borders.emailBorderColor}} onChange={(event) => this.handleUserInput(event)} className="form-control form-rounded"/>
                            </div>
                            <div className="col-md-3">
                            <label className="font-weight-bold">ZIP Code<b style={{color:"#ff2605"}}>*</b></label>
                            <input type="number" className="form-control form-rounded" name='zip' onChange={(event) => this.handleUserInput(event)} style={{borderColor:this.state.Borders.zipBorderColor}} value={this.state.vendorPostObject.zip} onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                        </div>
                        <br/>
                        <div style={{ border: '1px solid gray', borderRadius: '20px', marginLeft: '-15px',paddingLeft: '15px', width: '95%'}}>   
                        <div style={{marginTop:'-12px'}}>
                        <label className="font-weight-bold" style={{backgroundColor: 'white', marginLeft: '20px', padding: '0px 10px'}}>Remittance Address(if different than
                                        above)</label>     
                        </div>          
                        <div className="row mt-1"  style={{paddingBottom:'30px'}}>            
                            <div className="col-md-5">
                                <label className="font-weight-bold">Address</label>
                                <input type="text" value={this.state.vendorPostObject.remittanceAddress} onChange={(event) => this.handleUserInput(event)} name="remittanceAddress" className="form-control form-rounded"/>
                            </div>
                            <div className="col-md-3" style={{marginLeft: '-10px', marginRight: '-10px'}}> 
                                <label className="font-weight-bold">City</label>
                                <input type="text" value={this.state.vendorPostObject.remittanceCity} onChange={(event) => this.handleUserInput(event)} name="remittanceCity" className="form-control form-rounded" />
                            </div>
                            <div className="col-md-2">
                                <label className="font-weight-bold">State</label>
                                <select title="Pick a number" value={this.state.vendorPostObject.remittanceState} onChange={(event) => this.handleUserInput(event)} name="remittanceState" className="form-control form-rounded" >
                                    <option>Select...</option>
                                    <option>One</option>
                                    <option>Two</option>
                                    <option>Three</option>
                                </select>
                            </div>
                            <div className="col-md-2">
                                <label className="font-weight-bold">ZIP Code</label>
                                <input type="number" value={this.state.vendorPostObject.remittanceZip} onChange={(event) => this.handleUserInput(event)} name="remittanceZip" className="form-control form-rounded" />
                            </div>
                    </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-md-7">
                            <label><b>Signed W9 Form</b> - this will be required for new vendors/subs not found in JDE</label><br/>
                            <button type="button" className="btn btn-primary btn-sm" style={{borderRadius: '20px', padding: '7px 30px'}}><FontAwesomeIcon icon={faPaperclip} style={{fontSize:'18px', marginRight: '5px'}}/> Attach file</button>
                        </div>
                        <div className="col offset-md-3 mt-4" style={{paddingLeft: '1px'}}>
                            <button onClick={this.props.showVendorForm} className="button-rounded_submit" style={{width: '100px', height: '35px', borderRadius: '8px', backgroundColor: 'rgb(28, 51, 119)'}} onClick={this.ValidateForm}>Submit</button>
                        </div>
                    </div>
                    <div className="row mt-3"></div>
                    </div>                   
                </Form>
            </div>
            </div>
    );
   }
   
   ValidateForm = () => {
        const emailRegex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const requiredFields = [
            'companyName', 'city', 'zip', 'state', 'mailingAddress'
        ]

        const obj = this.state.vendorPostObject;
        Object.keys(obj).forEach(e => {
            if(requiredFields.some(x => x === e) && obj[e] === ''){
                this.setState((prevState) => ({
                        Borders: {                 
                            ...prevState.Borders,    
                            [e + 'BorderColor']: '#ff3e1c'
                        }       
                    }), () => {this.SubmitData()}
                )
            }
            else{
                this.setState((prevState) => ({
                    Borders: {                 
                        ...prevState.Borders,    
                        [e + 'BorderColor']: '#ced4da'
                    }
                }), () => {this.SubmitData()}
                )
            }
        });        
        const borders = this.state.Borders;
       if(emailRegex.test(this.state.vendorPostObject.email)){
            this.setState(prevState => ({
                Borders: {                 
                    ...prevState.Borders,    
                    emailBorderColor: '#ced4da'
                    }
                }))
       }
       else{
            this.setState(prevState => ({
                Borders: {                 
                    ...prevState.Borders,    
                    emailBorderColor: '#ff3e1c'
                    }
                }))
       }
   }

   SubmitData(){
    const borders = this.state.Borders;
    var requiredValuesPassed = Object.keys(borders).every(x => borders[x] === '#ced4da')
    if(requiredValuesPassed){
        if(this.state.FormTitle != 'Edit Vendor'){
            this.props.vendorActions.postVendorData(this.state.vendorPostObject)
            }
        else{
            this.props.vendorActions.updateVendorData(this.state.vendorUpdateObject)
            }
            this.props.showQueue();
        }      
   }
   handleChange(e){
    const name = e.target.name;
    const value = e.target.value;

        this.setState(prevState => ({
            vendorPostObject: {                 
                ...prevState.vendorPostObject,    
                [name]: value       
            }
        }))
        this.setState(prevState => ({
            vendorUpdateObject: {                 
                ...prevState.vendorUpdateObject,    
                [name]: value       
            }
        }))
        
        
    } 

   handleUserInput (e) {
    const name = e.target.name;
    const value = e.target.value;

    this.setState(prevState => ({
        vendorPostObject: {
            ...prevState.vendorPostObject,
            [name]: value
        }
    }))
    this.setState(prevState => ({
        vendorUpdateObject: {                 
            ...prevState.vendorUpdateObject,    
            [name]: value       
        }
    }))
  } 
}


function mapStateToProps({ createReducer }) {
    return {
      vendorObject: createReducer.VendorObject
    };
  }
  function mapDispatchToProps(dispatch){
      return{ 
          vendorActions: bindActionCreators(vendorActions, dispatch)
      };
  }
  export default connect(mapStateToProps, mapDispatchToProps) (registerForm);