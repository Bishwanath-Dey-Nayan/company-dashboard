import React, { useState } from 'react';
import { faDownload, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as  vendorActions from '../../actions/vendorData'
import { connect } from 'react-redux';
import './vendorForm.modules.css'
import moment from 'moment'
import { bindActionCreators } from 'redux';
const VendorForm = props =>{
    const [vendorNumber, setVendorNumber] = useState('');
        return (
            <div >
                <div className="container" style={{width: '60%'}}>
                <div className="row mt-4">
                        <div className="col-md-5 mt-3">
                        <button type="button" className="btn btn-outline-primary btn-sm" onClick={props.showQueueForm} style={{borderRadius: '20px'}}><FontAwesomeIcon icon={faArrowLeft} style={{fontSize:'14px'}}/> Back</button>
                        </div>
                </div>
                    <div className="row">
                        <div className="col-md-7 mt-3">
                            <h1 className="font-weight-bold" style={{fontFamily: 'boldFont'}}>Vendor Form Review</h1>
                            <br/>
                            <h4 className="font-weight-bold" style={{fontSize: '12px'}}>{props.VendorData.vendorNumber}</h4>
                        </div>
                        <div id='dateBar' className="col-md-4 offset-1 mt-3" style={{textAlign:'right', marginLeft: '55px'}}>
                            <label className="font-weight-bold">Date Submitted</label> 
                            <br/>
                            <label>{moment(props.VendorData.updateDate).format("dddd, MMMM Do, YYYY")}</label>
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-md-3">
                            <label className="font-weight-bold">Company Name:</label><br />
                            <label>{props.VendorData.companyName}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">Organization Type:</label><br />
                            <label>{props.VendorData.organizationType}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">Tax ID#</label><br style={{height: '5vh'}}/>
                            <label>{props.VendorData.taxIdNumber}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">Company</label><br style={{height: '5vh'}}/>
                            <label>GSI</label>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-md-3">
                            <label className="font-weight-bold">Address</label><br />
                            <label>{props.VendorData.mailingAddress}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">City</label><br />
                            <label>{props.VendorData.city}</label>
                        </div>
                        <div className="col-md-2">
                            <label className="font-weight-bold">State</label><br />
                            <label>{props.VendorData.state}</label>
                        </div>
                        <div className="col-md-2">
                            <label className="font-weight-bold">ZIP Code</label><br />
                            <label>{props.VendorData.zip}</label>
                        </div>
                    </div>

                    <div className="row mt-4">
                        <div className="col-md-3">
                            <label className="font-weight-bold">Contact Person</label><br />
                            <label>{props.VendorData.contactPerson}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">Business Phone</label><br />
                            <label>{props.VendorData.businessPhone}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">Fax</label><br />
                            <label>{props.VendorData.fax}</label>
                        </div>
                        <div className="col-md-3">
                            <label className="font-weight-bold">W9 Form</label><br />
                            <a href="#" className="btn btn-primary btn-sm form-rounded" style={{marginLeft: '-10px', backgroundColor :'rgb(28, 51, 119)'}}
                                role="button" aria-pressed="true"> <FontAwesomeIcon icon={faDownload} />&nbsp; Download</a>
                        </div>
                    </div>
                    
                    <div className="row mt-5">
                        <div className="col-md-5">
                            <label className="font-weight-bold">Email Address</label><br />
                            <label>{props.VendorData.email}</label>
                        </div>
                        <div className="col-md-4">
                            <label className="font-weight-bold">Remitttance Address</label><br />
                            <label>{props.VendorData.remittanceAddress}</label>
                        </div>
                        <div className="col-md-2">
                            <button type="button" onClick={() => props.showRegistrationEdit(props.VendorData)} className="button-rounded_edit"
                                style={{width: '80px', fontSize: '13px', fontWeight: '600', borderColor: 'rgb(28, 51, 119)', borderWidth: '2px', height: '35px', paddingLeft: '10px', paddingRight: '10px', paddingBottom: '3px'}}>Edit</button>
                    </div>
                    </div>
                    <hr style={{borderTop: '1px solid gray'}}/>
                </div>
                {!props.hideContainer && <div className="container bottomContainer" style={{width: '60%'}}>
                    <div className="row mt-3">
                        <div className="col-md-5 mt-3">
                            <button type="button" onClick={props.showRegistration} className="button-rounded_newVendor"
                                style={{width: '130px', fontSize: '13px', fontWeight: '600', borderColor: 'rgb(28, 51, 119)', borderWidth: '2px', height: '35px', paddingLeft: '10px', paddingRight: '10px', paddingBottom: '3px'}}>New Vendor</button>
                            <button className="button-rounded_changeVendor" style={{width: '130px', marginLeft: '-10px', backgroundColor : 'rgb(28, 51, 119)', height: '35px', paddingLeft: '10px', paddingRight: '10px', borderWidth: '0px', paddingBottom: '3px'}}
                                role="button" aria-pressed="true">Vendor change</button>
                        </div>
                        <div className="col-md-4" style={{marginTop:'-8px'}}>
                            <label style={{marginTop: '-5px', fontSize: '12px'}} className="font-weight-bold"> Vendor Number</label> <br />
                            <input type="text" onChange={(event) => setVendorNumber(event.target.value)} className="form-control form-rounded" style={{height: '29px'}}/>
                        </div>
                        <div className="col-md-2 offset-1 mt-3">
                            <button onClick={() => updateVendorNumber(vendorNumber, props)} className="button-rounded_submit" style={{width: '90px', height: '38px', borderRadius: '8px'}}>Submit</button>
                        </div>
                    </div>
                </div>}
                <div className="row mt-3"/>
            </div>
        );
    }

    const updateVendorNumber = (vendorNumber, props) =>{
        props.VendorData.vendorNumber = vendorNumber
        props.vendorActions.updateVendorData(props.VendorData);
    }
    function mapStateToProps({ createReducer }) {
        return {
          vendorObject: createReducer.VendorObject
        };
      }
      function mapDispatchToProps(dispatch){
          return{ 
              vendorActions: bindActionCreators(vendorActions, dispatch)
          };
      }
      export default connect(mapStateToProps, mapDispatchToProps) (VendorForm);