import { load } from 'redux-dataloader';

export const FETCH_VENDOR_DATA = 'FETCH_VENDOR_DATA';
export const POST_VENDOR_DATA = 'POST_VENDOR_DATA';
export const UPDATE_VENDOR_DATA = 'UPDATE_VENDOR_DATA';

export function fetchVendorData() {
  // use `load` to wrap a request action, load() returns a Promise
  return load({
    type: FETCH_VENDOR_DATA
  })
}

export function fetchVendorDataSuccess(data){
    return{
        type: 'FETCH_VENDOR_DATA_SUCCESS',
        data
    }   
}

export function postVendorData(data) {
  console.log(data)
  // use `load` to wrap a request action, load() returns a Promise
  return load({
    type: POST_VENDOR_DATA,
    postData:data
  })
}

export function postVendorDataSuccess(data){
    return{
        type: 'POST_VENDOR_DATA_SUCCESS',
        data
    }   
}

export function updateVendorData(data) {
  // use `load` to wrap a request action, load() returns a Promise
  return load({
    type: UPDATE_VENDOR_DATA,
    updateData:data
  })
}

export function updateVendorDataSuccess(data){
    return{
        type: 'UPDATE_VENDOR_DATA_SUCCESS',
        data
    }   
}