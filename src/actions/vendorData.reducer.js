import {createReducer} from '../utils'
const defaultState = {
    VendorObject: []
}

export const fetchVendorDataSuccess = (state, {data}) => ({
        ...state,
        VendorObject:data,
    }
);

export const postVendorDataSuccess = (state, {data}) => ({
    ...state
})

export const updateVendorDataSuccess = (state, {data}) => {
    // ...state,
    let { updateArray } = state;
    console.log('Update array here ->', updateArray);
    updateArray = state.data.map(item => {
        if (item.id === data.id) {
          return data;
        }
    
        return item;
      });
}

export default createReducer(defaultState, {
    'FETCH_VENDOR_DATA_SUCCESS': fetchVendorDataSuccess,
    'POST_VENDOR_DATA_SUCCESS' : postVendorDataSuccess,
    'UPDATE_VENDOR_DATA_SUCCESS' : updateVendorDataSuccess
});