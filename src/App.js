import React, {Component} from 'react';
import './App.css';
import { faPowerOff } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import VendorForm from './component/VendorForm/VendorForm';
import 'bootstrap/dist/css/bootstrap.css';
import QueueForm from './component/QueueForm/queueForm';
import RegisterForm from './component/RegisterForm/registerForm';
import logo from './assets/graycor-main.PNG'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as  vendorActions from './actions/vendorData'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { VendorFormCall: 0, Vendor: {}, showRegister: false, showVendor: false, showQueue: true, ParentIsQueue: true, hideContainer: false }
  }

  componentDidMount() {
    // Additionally I could have just used an arrow function for the binding `this` to the component...
    document.body.style.margin = "0px";
    this.props.vendorActions.fetchVendorData();  
  }
  render() {
   
    return (
      <div>
        <div className="topnav" id="myTopnav">
          <div className="topnav-right" style={{width: '90px', height: '38px'}}>
            <button className="button-logOut" style={{width: '90px', height: '30px', borderRadius: '2px', margin: '5px'}}><FontAwesomeIcon icon={faPowerOff} transform='rotate--90' style={{fontSize:'14px', marginRight: '8px'}}/>Log Out</button>
          </div>
          <a><img src={logo} style={{height: '41px'}}/></a>
          <a href="#home" className="active">DASHBOARD</a>
          <a className="active"><i className="arrows right"></i></a>
          <a href="#news" className="inactive-tab">ADMINISTRATIVE TASKS</a>
          <a className="active"><i className="arrows right"></i></a>
          <a href="#contact" className="inactive-tab">VENDOR FORMS</a>
        </div>
        {this.state.showRegister && <RegisterForm VendorData={this.state.Vendor} showQueue={this.showQueueForm}/>}
        {this.state.showQueue && <QueueForm vendorObject={this.props.vendorObject} showRegistrationFromQueue={this.showRegisterFormQueue} showVendorForm={this.showVendorForm} showVendorFormDisabled={this.showVendorFormDisabled}/>}
        {/* {this.state.showRegister && <RegisterForm showVendorForm={this.showVendorForm}/>} */}
        {this.state.showVendor && <VendorForm updateVendorNumber={this.updateVendorNumber} VendorData={this.state.Vendor} hideContainer={this.state.hideContainer} showRegistration={this.showRegisterForm} showRegistrationEdit={this.showRegisterFormEdit} showQueueForm={this.showQueueForm}/>}
      </div>
    );
  }

  closeModal = () => {
    this.setState({
      showRegister: false
    })
  }
  showRegisterForm = () => {
    this.setState({
      Vendor: undefined,
      showRegister: true,
      showVendor: false,
      showQueue: false
    })
  }

  showRegisterFormEdit = (dataFromChild) => {
    this.setState({
      Vendor: dataFromChild,
      showRegister: true,
      showVendor: false,
      showQueue: false
    });
    this.setState(prevState => ({
        Vendor: {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
            ...prevState.Vendor,    
            updateDate: new Date()       
        }
    }));
  }

  showRegisterFormQueue = () => {
    console.log('Show registered')
    this.showRegisterForm();
    this.setState({
      ParentIsQueue: true
    });
  }
    showVendorForm = (dataFromChild) => {
      this.setState({
        Vendor: dataFromChild ,
        hideContainer: false,
        showRegister: false,
        showVendor: true,
        showQueue: false
      })
      console.log('Showed')
    }
 
    // if (this.state.ParentIsQueue){
    //   this.setState({
    //     Vendor: dataFromChild,
    //     hideContainer: false,
    //     showRegister: false,
    //     showVendor: false,
    //     showQueue: true,
    //     ParentIsQueue: false
    //   });
    // }
    // else {
    //   this.setState({
    //     hideContainer: false,
    //     showRegister: false,
    //     showVendor: true,
    //     showQueue: false
    //   });
    //

  showVendorFormDisabled = () => {
      this.setState({
        hideContainer: true,
        showRegister: false,
        showVendor: true,
        showQueue: false
      });
  }

  showQueueForm = () => {
    this.props.vendorActions.fetchVendorData();
    console.log('Got data')
    this.setState({
      showRegister: false,
      showVendor: false,
      showQueue: true
    });
    
  }
  updateVendorNumber = (id, number) =>{
    console.log('update called')
    let dataFromChild = {
      id, number
    }
    this.showVendorForm(dataFromChild);
  }
  
}
function mapStateToProps({ createReducer }) {
  return {
    vendorObject: createReducer.VendorObject
  };
}
function mapDispatchToProps(dispatch){
    return{ 
        vendorActions: bindActionCreators(vendorActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps) (App);