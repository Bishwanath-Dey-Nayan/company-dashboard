import { createLoader } from 'redux-dataloader';
import * as vendorActions from '../actions/vendorData';

const config = require('Config')
export const vendorLoader = createLoader(vendorActions.FETCH_VENDOR_DATA, {
    fetch: async context =>{
        const query = 'query ($input:VendorDataTypeInputArgs){vendorItemQuery(input: $input) {vendorNumber,id,taxIdNumber,companyName,organizationType, mailingAddress,contactPerson,city,state,businessPhone,fax,email,zip,remittanceAddress,remittanceCity,remittanceState,remittanceZip,isProcessed,updateDate,updateUser}}';
        const response = await fetch(config['api-endpoint'], {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                query
              }),
        })
        .then(resp => resp.json())
        .then(result => result.data.vendorItemQuery)
        .catch(err => console.error(err))

        return {data: response};
    },

    success: ({dispatch}, {data}) => {
        dispatch(vendorActions.fetchVendorDataSuccess(data))
    }
});

export const insertData = createLoader(vendorActions.POST_VENDOR_DATA,
    {
      fetch: async context => {
        const { postData } = context.action;  
        console.log('Post data', postData)
        let query = `mutation createVendorItem($input: VendorDataCreateInput!){ createVendorItem(input: $input) {companyName,taxIdNumber,organizationType,mailingAddress,contactPerson,city,state,businessPhone,fax,email,zip,remittanceAddress,remittanceCity,remittanceState,remittanceZip}}`;
  
        postData.zip=parseInt(postData.zip,10);
        postData.remittanceZip=parseInt(postData.remittanceZip,10);
        const variables = {
          input: {
            ...postData,
          },
        };
  
        const response = await fetch(config['api-endpoint'], {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            query,
            variables,
          }),
        })
          .then(resp => {
            console.info('resp:', resp);
            return resp.json();
          })
          .then(results => {
            const { data } = results;
            return data;
          })
          .catch(err => console.error('Vendor POST error:', err)); // TODO - need a better way to handle error
  
        return { data: response };
      },
      success: ({ dispatch }, { data }) => {
        dispatch(vendorActions.postVendorDataSuccess(data));
        return; // eslint-disable-line
      },
    },
    
    {
      ttl: 1000,
      retryTimes: 0
    },
  );

  export const updateData = createLoader(vendorActions.UPDATE_VENDOR_DATA,
    {
      fetch: async context => {
        const { updateData } = context.action;  
        let query = `mutation updateVendorItem($input: VendorDataUpdateInput!){ updateVendorItem(input: $input)
            {id,isProcessed,companyName,taxIdNumber,organizationType,mailingAddress,contactPerson,city,state,
            businessPhone,fax,email,zip,remittanceAddress,remittanceCity,remittanceState,remittanceZip,updateDate,
            updateUser,vendorNumber}}`;
  
            updateData.zip=parseInt(updateData.zip,10);
            updateData.remittanceZip=parseInt(updateData.remittanceZip,10);
        const variables = {
          input: {
            ...updateData,
          },
        };
  
        const response = await fetch(config['api-endpoint'], {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            query,
            variables,
          }),
        })
          .then(resp => {
            return resp.json();
          })
          .then(results => {
            const { data } = results;
            return data;
          })
          .catch(err => console.error('Vendor UPDATE error:', err)); // TODO - need a better way to handle error
  
        return { data: response };
      },
      success: ({ dispatch }, { data }) => {

        dispatch(vendorActions.updateVendorDataSuccess(data));
        return; // eslint-disable-line
      },
    },
    
    {
      ttl: 1000,
      retryTimes: 0
    },
  );

export default [
    vendorLoader, insertData, updateData
  ];
  