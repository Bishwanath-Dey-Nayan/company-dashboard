import { combineReducers } from 'redux';
import createReducer from './actions/vendorData.reducer';
const rootReducer = combineReducers({
    createReducer
});
export default rootReducer;